import cv2 as cv
import cv2.aruco as aruco
import time
import yaml
import numpy as np
from math import sqrt
import requests
import json
import os


print("cv ver: ", cv.__version__)

# ---------- Load camera intrinsics ----------
print("*************************************************************************")
print("Loading camera calibration parameters...")

# Load camera intrinsics from yaml file
# fname = 'Nikon_D3300_intrinsics_1280x720.yaml'
fname = 'Logitech_C270_intrinsics_1280x720.yaml'

with open(fname) as file:
    camera_intrinsics = yaml.load(file)[0]

DESIRED_IMAGE_HEIGHT = camera_intrinsics['des_hight']
cameraMatrix = np.array(camera_intrinsics['camera_matrix'])
distCoeffs = np.array(camera_intrinsics['dist_coeff'])
params = camera_intrinsics['params']

print("RMS re-projection error: %.2f" % camera_intrinsics['rms'])
print("camera_intrinsics['des_hight']: ", camera_intrinsics['des_hight'])
print("cameraMatrix:\n {0}".format(cameraMatrix))
print("distCoeffsr:\n {0}".format(distCoeffs))

cameraMatrix_inv = np.linalg.inv(cameraMatrix)

# ---------- Photo: D3300_1280x720_floor.jpg ----------
img_file = 'D3300_1280x720_floor.jpg' #
imagePoints = [[425., 245.], [266., 661.], [981., 633.], [752., 235.]]
objectPoints = [[0., 0., 0.], [0.,5697.,0.], [-2123.,5707.,0.], [-2120.,0.,0.]]
# Point 1 (over the white box)
# uvPoint = [[2485.], [870.], [1.]]
# Zconst = 150
# P_real = [[-1050.], [707.], [150.]]

# Point 2 (over the floor)
uvPoint = [[733.], [325.], [1.]] # 
Zconst = 0.
P_real = [[-1763.], [2138.], [0.]]

# Scale imagePoints and uvPoint values to resized image
imagePoints = np.array(imagePoints) * params['scaling_factor']
objectPoints = np.array(objectPoints)
uvPoint = np.array([[uvPoint[0][0]*params['scaling_factor']], [uvPoint[1][0]*params['scaling_factor']], [1.]])

# print("tf ver: ", tf.__version__) # tf ver. 1.5.0

video_file = "../videos/TownCentreXVID.avi"
    
cap = cv.VideoCapture(video_file)
# cap = cv.VideoCapture(0) # Change to this for using the webcam

# Works! 0.72 sec.
weights_file = 'models/ssd_mobilenet_v1_coco_2017_11_17/frozen_inference_graph.pb'
config_file =  'models/ssd_mobilenet_v1_coco_2017_11_17.pbtxt'

# Works! 1.0 sec.
# weights_file = 'models/ssd_mobilenet_v2_coco_2018_03_29/frozen_inference_graph.pb'
# config_file =  'models/ssd_mobilenet_v2_coco_2018_03_29.pbtxt'

# Works! 2.6 sec.
# weights_file = 'models/ssd_inception_v2_coco_2017_11_17/frozen_inference_graph.pb'
# config_file =  'models/ssd_inception_v2_coco_2017_11_17.pbtxt'

# Works! 7.5 sec.
# weights_file = 'models/faster_rcnn_inception_v2_coco_2018_01_28/frozen_inference_graph.pb'
# config_file =  'models/faster_rcnn_inception_v2_coco_2018_01_28.pbtxt'
# config_file =  'models/out.pbtxt'

# Works! 42.0 sec.
# weights_file = 'models/faster_rcnn_resnet50_coco_2018_01_28/frozen_inference_graph.pb'
# config_file =  'models/faster_rcnn_resnet50_coco_2018_01_28.pbtxt'

# Didn't work
# weights_file = 'models/ssd_mobilenet_v3_large_coco_2020_01_14/frozen_inference_graph.pb'
# config_file =  'models/ssd_mobilenet_v3_large_coco_2020_01_14.pbtxt'

json_eu_distances_string = None
json_detected_points_string = None
too_close = 0

def calc_point(rotationMatrix, cameraMatrix, uvPoint, tvec):
    # global rotationMatrix_inv
    # global cameraMatrix_inv
    # print("---------------- leftSideMat, rightSideMat -----------------------------")
    Zconst = P_real[2][0]
    rotationMatrix_inv = np.linalg.inv(rotationMatrix)
    cameraMatrix_inv = np.linalg.inv(cameraMatrix)

    lsm1 = np.dot(rotationMatrix_inv, cameraMatrix_inv)
    leftSideMat  = np.dot(lsm1, uvPoint)
    rightSideMat = np.dot(rotationMatrix_inv, tvec)

    s = (Zconst + rightSideMat[2][0])/leftSideMat[2][0]

    # print("---------------- P_calc -----------------------------")
    p2 = np.dot((s * cameraMatrix_inv), uvPoint) - tvec

    P_calc = np.dot(rotationMatrix_inv, p2)

    return P_calc

while True:
    r, frame = cap.read()
    if r:
        # Get unix time
        unix_time = time.time()
        print("unix_time: ", unix_time)
        # Draw imagePoints
        for x, y in imagePoints:
            x_c = int(round(x))
            y_c = int(round(y))
            # print("point: " +  str(x_c) + ", " + str(y_c))
            cv.circle(frame, (x_c, y_c), 3, (0, 255, 0), 3)

        # Draw uvPoint
        x_c = int(round(uvPoint[0][0]))
        y_c = int(round(uvPoint[1][0]))
        cv.circle(frame, (x_c, y_c), 3, (0, 20, 255), 3)

        # Process
        # print("---------------- solvePnP -----------------------------")
        success, rvec, tvec = cv.solvePnP(objectPoints, imagePoints, cameraMatrix, distCoeffs)
        # Convert from (R, P, Y) Euler angles to rotation matrix
        rotationMatrix, _ = cv.Rodrigues(rvec)
        rotationMatrix_inv = np.linalg.inv(rotationMatrix)

        P_calc = calc_point(rotationMatrix, cameraMatrix, uvPoint, tvec)

        # print("---------------- Homography -----------------------------")
        aruco.drawAxis(frame, cameraMatrix, distCoeffs , rvec, tvec, frame.shape[1]*0.10)

        start_time = time.time()
        # frame = cv.resize(frame,(640,360)) # Downscale to improve frame rate
        img = frame
        rows = img.shape[0]
        cols = img.shape[1]
        cvNet.setInput(cv.dnn.blobFromImage(img, size=(300, 300), swapRB=True, crop=False))
        cvOut = cvNet.forward()
        delta_time = end_time = time.time()
        print("Elapsed Time:", end_time-start_time)

        detected_points = []
        # detected_points.append({'ut':unix_time})

        detected_points2 = []
        # detected_points2.append({'ut':unix_time})

        for detection in cvOut[0,0,:,:]:
            score = float(detection[2])
            # print("detection: " , detection)
            
            # extract the index of the class label from the
            # detections list
            #idx = int(detections[0, 0, i, 1])
            idx = int(detection[1])

            min_score = 0.25
            if (idx == 1) and score > min_score:
                left = detection[3] * cols
                top = detection[4] * rows
                right = detection[5] * cols
                bottom = detection[6] * rows
                h_center = left + (right-left)/2

                # print("left right top bottom: " + str(left) + " " + str(right) + " " + str(top) + " " + str(bottom))

                # Sometimes 'detectin[]' gives negative! That's why we must compute abs()
                left = abs(int(round(left)))
                top = abs(int(round(top)))
                right = abs(int(round(right)))
                bottom = abs(int(round(bottom)))
                h_center = abs(int(round(h_center)))

                # print("left right top bottom: " + str(left) + " " + str(right) + " " + str(top) + " " + str(bottom))

                rect_color = (0, 255, 0) # Green

                img[top:bottom, left:right] = cv.blur(img[top:bottom, left:right] ,(23,23))

                # img[10:100, 10:100] = cv.blur(img[10:100, 10:100] ,(23,23))
                cv.rectangle(img, (left, top), (right, bottom), rect_color, thickness=2)
                cv.putText(img, str(round(score, 2)), 
                        # (int(right), int(top)), cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1)
                        (left, bottom+5), cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1)
                cv.circle(img, (h_center, bottom), 2, (0, 0, 255), -1)

                # uvPoint = [[733.], [325.], [1.]]
                detect_uvPoint = np.array([[h_center], [bottom], [1.]])
                detect_xyzPoint = calc_point(rotationMatrix, cameraMatrix, detect_uvPoint, tvec)
                # print("detect_xyzPoint: ", detect_xyzPoint)
                # print("detect_xyzPoint[0][0]: ", detect_xyzPoint[0][0])
                # print("detect_xyzPoint[1][0]: ", detect_xyzPoint[1][0])

                # detected_points.append([detect_uvPoint, detect_xyzPoint])
                detected_points.append({'ut':unix_time, 'uv':detect_uvPoint, 'xyz':detect_xyzPoint})
                
                detected_points2.append({'u':round(h_center), 'v':round(bottom), 
                    'x':round(detect_xyzPoint[0][0]), 'y':round(detect_xyzPoint[1][0]),
                    'z':round(detect_xyzPoint[2][0])})

        json_detected_points_string = json.dumps({'t_stamp':unix_time, 'points':detected_points2})

        # draw distance lines
        detected_len = len(detected_points)
        # print("detected_len: ", detected_len)

        eu_distances = []
        # eu_distances.append({'ut':unix_time})

        MIN_PHYS_DIST = 1500.

        trackers_full = True

        too_close = 0

        for idx_a in range(0, detected_len-1):
            # print("idx: ", idx)

            for idx_b in range(idx_a+1, detected_len):
                # Euclidean distance
                P_delta = detected_points[idx_a]['xyz'] - detected_points[idx_b]['xyz']
                eu_dist = sqrt(P_delta[0]**2 + P_delta[1]**2)
                # print("eu_dist = ", eu_dist)

                # Draw lines <= MAX_PHYS_DIST
                MAX_PHYS_DIST = 2*MIN_PHYS_DIST

                if eu_dist <= MAX_PHYS_DIST:
                    too_close = too_close + 1
                    if eu_dist <= MIN_PHYS_DIST:
                        line_color = (0, 128, 255)
                    else:
                        line_color = (50, 128, 0)

                    p1 = (detected_points[idx_a]['uv'][0], detected_points[idx_a]['uv'][1])
                    p2 = (detected_points[idx_b]['uv'][0], detected_points[idx_b]['uv'][1])
                    cv.line(img, p1, p2, line_color, 1)

                    # coordinates of the midpoint of a segment
                    x_m = (p1[0] + p2[0]) / 2.
                    y_m = (p1[1] + p2[1]) / 2.
                    cv.putText(frame, str(round(eu_dist/1000., 1)), (x_m, y_m), 
                        cv.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 1)

                # eu_distances.append([idx_a, idx_b, eu_dist])
                eu_distances.append({'a':idx_a, 'b':idx_b, 'dist':round(eu_dist)})

        json_eu_distances_string = json.dumps({'t_stamp':unix_time, 'eu_distances':eu_distances})

        # (n k) = n! / k!(n-k)! | n(n-1)/2
        pairs = detected_len*(detected_len - 1)/2
        indexes = {'t_stamp':unix_time, 'ped_count':detected_len, 
                'pairs':pairs, 'close':too_close, 'close_pcent': round(too_close/pairs, 4)}
        json_indexes_string = json.dumps(indexes)
        print("indexes: ", indexes)

        # Resize to fit in desktop and show
        dimension = (int(params['image_width']*0.75), int(params['image_height']*0.75))
        img = cv.resize(img, dimension, interpolation = cv.INTER_AREA)
        cv.imshow('img', img)

        # WARNING: file will be saved in /home/pi/.ros (hidden dir) if full path not specified
        # check it with cwd = os.getcwd()
        # root_dir = self.rospack.get_path('opencv_tracking')
        dimension = (int(params['image_width']*0.20), int(params['image_height']*0.20))
        ped_img = cv.resize(img, dimension, interpolation = cv.INTER_AREA)

        # print(os.path.dirname(os.path.realpath(__file__)))
        root_dir = os.path.dirname(os.path.realpath(__file__))
        ret_val = cv.imwrite(root_dir + '/ped_tracking_frame.jpg', ped_img)

        if not ret_val:
            print('[OPENCV NODE] Error while saving camera shot!')
        else:
            print('[OPENCV NODE] Camera shot saved...')

        # print("eu_distances: ", eu_distances)

        # ---------- Sending JSON indexes ----------
        print('[HTTP CLIENT] Sending JSON indexes...')
        try:
            headers = {'User-Agent': "Mozilla Firefox", 'Content-type': 'application/xml', 
                        'Accept': 'text/plain'}
            r = requests.post('http://tec.bo/covid19-challenge/receive_indexes_json.php', 
                                data=json_indexes_string, headers=headers)

            # print(r.request.body)
            # print(r.status_code, r.reason)
            # print(r.text[:1500] + '...')
            # print(r.text)

        except Exception:
            print('Exception: Sending JSON indexes error!')

        # ---------- Sending JSON indexes ----------
        print('[HTTP CLIENT] Sending JSON points...')
        try:
            headers = {'User-Agent': "Mozilla Firefox", 'Content-type': 'application/xml', 
                        'Accept': 'text/plain'}
            r = requests.post('http://tec.bo/covid19-challenge/receive_points_json.php', 
                                data=json_detected_points_string, headers=headers)

            # print(r.request.body)
            print(r.status_code, r.reason)
            # print(r.text[:1500] + '...')
            print(r.text)

        except Exception:
            print('Exception: Sending JSON indexes error!')

        print('[HTTP CLIENT] Sending POST fire_tracking_frame.jpg request...')
        try:
            root_dir = os.path.dirname(os.path.realpath(__file__))

            files = {'media': open(root_dir + '/ped_tracking_frame.jpg', 'rb')}
            headers = {'User-Agent': "Mozilla Firefox"}
            r = requests.post('http://tec.bo/covid19-challenge/receive_image.php', 
                                files=files, headers=headers)

            print(r.status_code, r.reason)

            # print(r.text[:1500] + '...')
            # print(r.text)

        except Exception:
            print('Exception: image HTTP POST error!')
    else:
        break
    
    k = cv.waitKey(1)
    if k & 0xFF == ord("q"): # Exit condition
        break
    
cv.destroyAllWindows()