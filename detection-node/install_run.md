## TENSORFLOW INSTALLATION UBUNTU 18.04
https://www.liquidweb.com/kb/how-to-install-tensorflow-on-ubuntu-18-04/

Error:
Illegal instruction (core dumped)

https://github.com/tensorflow/tensorflow/issues/17411

Try running
pip uninstall tensorflow
And then
pip install tensorflow==1.5

EDIT
just to give credit, solution is from here:
https://stackoverflow.com/questions/49094597/illegal-instruction-core-dumped-after-running-import-tensorflow

--------------------------------------------------------------------------------
## INSTALL PYTHON OPENCV UBUNTU 18.04
https://linuxize.com/post/how-to-install-opencv-on-ubuntu-18-04/

To install OpenCV from the Ubuntu 18.04 repositories, follow these steps:

--------------------------------------------------------------------------------
## TENSORFLOW 2.0 INSTALLATION UBUNTU 18.04
Based on: https://www.pyimagesearch.com/2019/12/09/how-to-install-tensorflow-2-0-on-ubuntu/

sudo apt-get update
sudo apt-get upgrade

sudo apt-get install libatlas-base-dev

Let’s download and install pip:

wget https://bootstrap.pypa.io/get-pip.py
sudo python3 get-pip.py

pip3 install virtualenv virtualenvwrapper

Error:
bash: /usr/bin/pip3: No such file or directory

Solution:
sudo pip3 install virtualenv virtualenvwrapper

Note: Your system may require that you use the sudo
  command to install the above virtual environment tools. This will only be required once — from here forward, do not use sudo

Open up the ~/.bashrc
  file with Nano or another text editor:

$ nano ~/.bashrc

And insert the following lines at the end of the file:

# virtualenv and virtualenvwrapper
export WORKON_HOME=$HOME/.virtualenvs
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
source /usr/local/bin/virtualenvwrapper.sh

Go ahead and source/load the changes into your profile:

$ source ~/.bashrc

Error: bash: /usr/local/bin/virtualenvwrapper.sh: No such file or directory

Solution:
which virtualenvwrapper.sh
/home/raul/.local/bin/virtualenvwrapper.sh

Change: source /home/raul/.local/bin/virtualenvwrapper.sh
	source /home/pi/.local/bin/virtualenvwrapper.sh # RPi

Now we’re ready to create your Python 3 deep learning virtual environment named dl4cv:

$ mkvirtualenv dl4cv -p python3

Simply activate the environment with the following command:

$ workon dl4cv

Go ahead and install NumPy and TensorFlow 2.0 using pip:
$ pip install numpy
$ pip install tensorflow==2.0.0 # or tensorflow-gpu==2.0.0

We begin by installing standard image processing libraries including OpenCV:

$ pip install opencv-contrib-python

From there, let’s install machine learning libraries and support libraries, the most notable two being scikit-learn and matplotlib:
How to install TensorFlow 2.0 on Ubuntu

$ pip install matplotlib

Fire up a Python shell in your dl4cv
environment and ensure that you can import the following packages:
How to install TensorFlow 2.0 on Ubuntu
$ workon dl4cv
$ python
>>> import tensorflow as tf
>>> tf.__version__
2.0.0
>>> import tensorflow.keras
>>> import cv2
>>> cv2.__version__
4.1.2

Error:
import tensorflow as tf
Illegal instruction (core dumped)

Solution:
Try running
pip uninstall tensorflow
And then
pip install tensorflow==1.5

EDIT
just to give credit, solution is from here:
https://stackoverflow.com/questions/49094597/illegal-instruction-core-dumped-after-running-import-tensorflow
.......

Accessing your TensorFlow 2.0 virtual environment

$ workon dl4cv

If you need to get back to your system-level environment, you can deactivate the current virtual environment:

$ deactivate

--------------------------------------------------------------------------------
## TENSORFLOW INSTALLATION RASPBERRY PI
https://magpi.raspberrypi.org/articles/tensorflow-ai-raspberry-pi

sudo apt install libatlas-base-dev
pip3 install tensorflow


python tf_text_graph_faster_rcnn.py --input /path/to/model.pb --config /path/to/example.

python tf_text_graph_faster_rcnn.py --input models/faster_rcnn_inception_v2_coco_2018_01_28/frozen_inference_graph.pb --config configs/faster_rcnn_inception_v2_coco.config --output out.pbtxt

python tf_text_graph_ssd.py --input models/ssd_mobilenet_v3_large_coco_2020_01_14/frozen_inference_graph.pb --config configs/faster_rcnn_inception_v2_coco.config --output out2.pbtxt

--------------------------------------------------------------------------------
## VIRTUALENV INSTALLATION RASPBERRY PI
https://www.pyimagesearch.com/2019/09/16/install-opencv-4-on-raspberry-pi-4-and-raspbian-buster/

You can install pip using the following commands:
$ wget https://bootstrap.pypa.io/get-pip.py
$ sudo python get-pip.py
$ sudo python3 get-pip.py
$ sudo rm -rf ~/.cache/pip

Let’s install virtualenv  and virtualenvwrapper  now:
$ sudo pip install virtualenv virtualenvwrapper

Once both virtualenv  and virtualenvwrapper  have been installed, open up your ~/.bashrc  file:
$ nano ~/.bashrc

…and append the following lines to the bottom of the file:
# virtualenv and virtualenvwrapper
export WORKON_HOME=$HOME/.virtualenvs
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
source /usr/local/bin/virtualenvwrapper.sh

From there, reload your ~/.bashrc  file to apply the changes to your current bash session:
$ source ~/.bashrc

Next, create your Python 3 virtual environment:
$ mkvirtualenv dl4cv -p python3

ERROR: Environment '/home/pi/.virtualenvs/dl4cv' does not contain an activate script.

https://stackoverflow.com/questions/60252119/error-environment-users-myuser-virtualenvs-iron-does-not-contain-activation-s/60292344#60292344
I had the same error message when I used mkvirtualenv on a new RPI4. I added these lines to my .bashrc and it fixed the problem for me:
Add:
export VIRTUALENVWRAPPER_ENV_BIN_DIR=bin  # <== This line fixed it for me

--------------------------------------------------------------------------------
## TENSORFLOW LITE INTERPRETER INSTALLATION RASPBERRY PI
https://www.tensorflow.org/lite/guide/python

pip3 install https://dl.google.com/coral/python/tflite_runtime-2.1.0.post1-cp37-cp37m-linux_armv7l.whl

**Run classify picamera**
https://github.com/tensorflow/examples/tree/master/lite/examples/image_classification/raspberry_pi



Error:
Original error was: libf77blas.so.3: cannot open shared object file: No such file or directory

Solution:
https://numpy.org/devdocs/user/troubleshooting-importerror.html
The solution will be to either:

sudo apt-get install libatlas-base-dev

### RUN OBJECT RECOGNITION
cd examples/lite/examples/object_detection/raspberry_pi

# The script takes an argument specifying where you want to save the model files
bash download.sh /tmp

# Run the example
python3 detect_picamera.py \
  --model /tmp/detect.tflite \
  --labels /tmp/coco_labels.txt

--------------------------------------------------------------------------------
## OPENCV INSTALLATION RASPBERRY PI
https://www.pyimagesearch.com/2019/09/16/install-opencv-4-on-raspberry-pi-4-and-raspbian-buster/

### pip install OpenCV 4
The following pre-requisites are for Step #4a and they certainly won’t hurt for Step #4b either. They are for HDF5 datasets and Qt GUIs:

Install OpenCV 4 on Raspberry Pi 4 and Raspbian Buster
$ sudo apt-get install libhdf5-dev libhdf5-serial-dev libhdf5-103
$ sudo apt-get install libqtgui4 libqtwebkit4 libqt4-test python3-pyqt5

### Compile OpenCV 4 from source

The first step is to update and upgrade any existing packages:
$ sudo apt-get update && sudo apt-get upgrade

We then need to install some developer tools, including CMake, which helps us configure the OpenCV build process:
$ sudo apt-get install build-essential cmake pkg-config

Next, we need to install some image I/O packages that allow us to load various image file formats from disk. Examples of such file formats include JPEG, PNG, TIFF, etc.:
$ sudo apt-get install libjpeg-dev libtiff5-dev libjasper-dev libpng-dev

Just as we need image I/O packages, we also need video I/O packages. These libraries allow us to read various video file formats from disk as well as work directly with video streams:
$ sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
$ sudo apt-get install libxvidcore-dev libx264-dev

The OpenCV library comes with a sub-module named highgui which is used to display images to our screen and build basic GUIs. In order to compile the highgui module, we need to install the GTK development library and prerequisites:
$ sudo apt-get install libfontconfig1-dev libcairo2-dev
$ sudo apt-get install libgdk-pixbuf2.0-dev libpango1.0-dev
$ sudo apt-get install libgtk2.0-dev libgtk-3-dev

Many operations inside of OpenCV (namely matrix operations) can be optimized further by installing a few extra dependencies:
$ sudo apt-get install libatlas-base-dev gfortran

Lastly, let’s install Python 3 header files so we can compile OpenCV with Python bindings:
$ sudo apt-get install python3-dev

--------------------------------------------------------------------------------
## REQUIRED LIBRARIES
No module named 'dicttoxml'
 pip install dicttoxml