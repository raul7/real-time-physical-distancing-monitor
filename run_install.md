--------------------------------------------------------------------------------
## VIRTUALENV INSTALLATION RASPBERRY PI
https://www.pyimagesearch.com/2019/09/16/install-opencv-4-on-raspberry-pi-4-and-raspbian-buster/

You can install pip using the following commands:
$ wget https://bootstrap.pypa.io/get-pip.py
$ sudo python get-pip.py
$ sudo python3 get-pip.py
$ sudo rm -rf ~/.cache/pip

Let’s install virtualenv  and virtualenvwrapper  now:
$ sudo pip install virtualenv virtualenvwrapper

Once both virtualenv  and virtualenvwrapper  have been installed, open up your ~/.bashrc  file:
$ nano ~/.bashrc

…and append the following lines to the bottom of the file:
# virtualenv and virtualenvwrapper
export WORKON_HOME=$HOME/.virtualenvs
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
source /usr/local/bin/virtualenvwrapper.sh

From there, reload your ~/.bashrc  file to apply the changes to your current bash session:
$ source ~/.bashrc

Next, create your Python 3 virtual environment:
$ mkvirtualenv dl4cv -p python3

ERROR: Environment '/home/pi/.virtualenvs/dl4cv' does not contain an activate script.

https://stackoverflow.com/questions/60252119/error-environment-users-myuser-virtualenvs-iron-does-not-contain-activation-s/60292344#60292344
I had the same error message when I used mkvirtualenv on a new RPI4. I added these lines to my .bashrc and it fixed the problem for me:
Add:
export VIRTUALENVWRAPPER_ENV_BIN_DIR=bin  # <== This line fixed it for me

--------------------------------------------------------------------------------
## OPENCV INSTALLATION RASPBERRY PI
https://www.pyimagesearch.com/2019/09/16/install-opencv-4-on-raspberry-pi-4-and-raspbian-buster/

### pip install OpenCV 4
The following pre-requisites are for Step #4a and they certainly won’t hurt for Step #4b either. They are for HDF5 datasets and Qt GUIs:

Install OpenCV 4 on Raspberry Pi 4 and Raspbian Buster
$ sudo apt-get install libhdf5-dev libhdf5-serial-dev libhdf5-103
$ sudo apt-get install libqtgui4 libqtwebkit4 libqt4-test python3-pyqt5

### Compile OpenCV 4 from source

The first step is to update and upgrade any existing packages:
$ sudo apt-get update && sudo apt-get upgrade

We then need to install some developer tools, including CMake, which helps us configure the OpenCV build process:
$ sudo apt-get install build-essential cmake pkg-config

Next, we need to install some image I/O packages that allow us to load various image file formats from disk. Examples of such file formats include JPEG, PNG, TIFF, etc.:
$ sudo apt-get install libjpeg-dev libtiff5-dev libjasper-dev libpng-dev

Just as we need image I/O packages, we also need video I/O packages. These libraries allow us to read various video file formats from disk as well as work directly with video streams:
$ sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
$ sudo apt-get install libxvidcore-dev libx264-dev

The OpenCV library comes with a sub-module named highgui which is used to display images to our screen and build basic GUIs. In order to compile the highgui module, we need to install the GTK development library and prerequisites:
$ sudo apt-get install libfontconfig1-dev libcairo2-dev
$ sudo apt-get install libgdk-pixbuf2.0-dev libpango1.0-dev
$ sudo apt-get install libgtk2.0-dev libgtk-3-dev

Many operations inside of OpenCV (namely matrix operations) can be optimized further by installing a few extra dependencies:
$ sudo apt-get install libatlas-base-dev gfortran

Lastly, let’s install Python 3 header files so we can compile OpenCV with Python bindings:
$ sudo apt-get install python3-dev

--------------------------------------------------------------------------------
## REQUIRED LIBRARIES
pip install pyyaml
pip install json
pip install requests