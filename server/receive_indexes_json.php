<?php
	// Definir zona horaria para impresion correcta de fechas 
	date_default_timezone_set("America/La_Paz");  

	// Conectarse a la base de datos
	require_once 'login.php'; // Incluir script que contiene info para inicio de sesion en la base de datos
	

	echo "----------------------------\r\n";
    $server_name= gethostname();
    $server_ip = $_SERVER['SERVER_ADDR'];  //getHostByName(getHostName());
    echo "Receiving JSON indexes data at: " . $server_name. " | " . $server_ip . "\r\n";

    $jsonString = trim(file_get_contents('php://input'));

    $jsonObject = json_decode($jsonString);
    var_dump($jsonObject);
    echo $jsonObject->{'ped_count'};


	// ---- Save raw telemetry XML data to file
	$fp = fopen('indexes_data.json', 'w');
	$ret_val = fwrite($fp, $jsonString);
	fclose($fp);

	if ($ret_val) {
		echo "[JSON indexes] indexes data saved to file.\r\n";
	}
	else {
		echo "[JSON indexes] Error while saving indexes data to file.\r\n";
	}

	$conexion = new mysqli($hn, $un, $pw, $db);
	if ($conexion->connect_error) die($conexion->connect_error);

	$ts = $jsonObject->{'t_stamp'};
    $pc = $jsonObject->{'ped_count'};
    $pa = $jsonObject->{'pairs'};
    $cl = $jsonObject->{'close'};
    $cp = $jsonObject->{'close_pcent'};

    // Construir cadena de texto para la consulta a la base de datos
	$consulta    = "INSERT INTO distancing (t_stamp, ped_count, pairs, close, close_pcent) VALUES" .
	  "('$ts', '$pc', '$pa', '$cl', '$cp')";

	// Enviar consulta a la base de datos
	$resultado   = $conexion->query($consulta);

	// Si la consulta fallo, imprimir mensaje en la pagina
	if (!$resultado) echo "INSERT fallo: $consulta<br>" .
	  $conexion->error . "<br><br>";

?>