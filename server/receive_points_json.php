<?php

	echo "----------------------------\r\n";
    $server_name= gethostname();
    $server_ip = $_SERVER['SERVER_ADDR'];  //getHostByName(getHostName());
    echo "Receiving points JSON data at: " . $server_name. " | " . $server_ip . "\r\n";

    //
    $jsonString = trim(file_get_contents('php://input'));

	// ---- Save raw telemetry XML data to file
	$fp = fopen('points_data.json', 'w');
	$ret_val = fwrite($fp, $jsonString);
	fclose($fp);

	if ($ret_val) {
		echo "[JSON data] points JSON data saved to file.\r\n";
	}
	else {
		echo "[JSON data] Error while saving points JSON data to file.\r\n";
	}

?>