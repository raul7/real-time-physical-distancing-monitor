<?php
	// Conectarse a la base de datos
	require_once 'login.php'; // Incluir script que contiene info para inicio de sesion en la base de datos

	$limit = 30;

	$conexion = new mysqli($hn, $un, $pw, $db);
	if ($conexion->connect_error) die($conexion->connect_error);

	$consulta  = "SELECT * FROM (SELECT * FROM distancing ORDER BY id DESC LIMIT " . $limit . ") sub ORDER BY id ASC"; // Cadena de consulta

	$resultado = $conexion->query($consulta);

	$to_encode = array();

	if (!$resultado) die($conexion->error); // Si la consulta falla imprimir error

	$filas = $resultado->num_rows; // Obtener numero de registros

	$min_idx = 0; // De lo contrario, imprimir todos los registros desde el primero

	// Iterar sobre los registros a imprimir
	for ($j = $min_idx ; $j < $filas ; ++$j)
	{
		$resultado->data_seek($j); // Mover el puntero al primer registro a imprimir
		$fila = $resultado->fetch_array(MYSQLI_ASSOC); // Recabar todo el registro

		$to_encode[] = $fila;
	}
	
	// Cerrar conexion con la base de datos
	$resultado->close();
	$conexion->close();

	echo json_encode($to_encode);
?>

