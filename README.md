## Real-Time Physical Distancing Monitor

**Computes "crowdedness" indexes in public areas using computer vision with the purpose of prevention and evaluation of social distancing.**

This is my submission to the COVID-19 Detect & Protect Challenge.

The main idea and the prototype is fully working, but it still should be considered a work in progress.